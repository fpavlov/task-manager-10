package ru.t1.fpavlov.tm.model;

import java.util.UUID;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class Project {

    private String id = UUID.randomUUID().toString();

    private String name = "";

    private String description = "";

    public Project() {
    }

    public Project(String name) {
        this.name = name;
    }

    public Project(String name, String description) {
        this.name = name;
        this.description = description;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return this.name + "\t-\t" + this.description;
    }

}
