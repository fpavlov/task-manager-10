package ru.t1.fpavlov.tm.util;

import java.util.Scanner;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface TerminalUtil {

    Scanner SCANNER = new Scanner(System.in);

    static String nextLine() {
        return SCANNER.nextLine();
    }

}
