package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.ITaskRepository;
import ru.t1.fpavlov.tm.api.ITaskService;
import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public Task add(final Task task) {
        if (task == null) return null;
        return this.taskRepository.add(task);
    }

    @Override
    public Task create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return this.add(new Task(name));
    }

    @Override
    public Task create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return this.add(new Task(name, description));
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

    @Override
    public List<Task> findAll() {
        return this.taskRepository.findAll();
    }

}
