package ru.t1.fpavlov.tm.service;

import ru.t1.fpavlov.tm.api.*;
import ru.t1.fpavlov.tm.model.Command;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return this.commandRepository.getTerminalCommands();
    }

}
