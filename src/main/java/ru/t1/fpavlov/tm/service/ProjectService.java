package ru.t1.fpavlov.tm.service;


import ru.t1.fpavlov.tm.api.*;
import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectService implements IProjectService {


    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public Project add(final Project project) {
        if (project == null) return null;
        return this.projectRepository.add(project);
    }

    @Override
    public Project create(final String name) {
        if (name == null || name.isEmpty()) return null;
        return this.add(new Project(name));
    }

    @Override
    public Project create(final String name, final String description) {
        if (name == null || name.isEmpty()) return null;
        if (description == null || description.isEmpty()) return null;
        return this.add(new Project(name, description));
    }

    @Override
    public void clear() {
        this.projectRepository.clear();
    }

    @Override
    public List<Project> findAll() {
        return this.projectRepository.findAll();
    }

}
