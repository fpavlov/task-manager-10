package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.*;
import ru.t1.fpavlov.tm.model.Project;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void create() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Project item = projectService.create(name, description);
        if (item == null) {
            System.out.println("\tError!");
        } else {
            System.out.println("\tOk");
        }
    }

    @Override
    public void clear() {
        System.out.println("Clear projects");
        projectService.clear();
        System.out.println("\tOk");
    }

    @Override
    public void show() {
        int index = 1;
        final List<Project> items = projectService.findAll();
        System.out.println("Projects list:");
        for (final Project item : items) {
            System.out.println("\t\t" + index + ".\t" + item);
            index++;
        }
        System.out.println("\tOk");
    }

}
