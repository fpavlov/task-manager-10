package ru.t1.fpavlov.tm.controller;

import ru.t1.fpavlov.tm.api.*;
import ru.t1.fpavlov.tm.model.Task;
import ru.t1.fpavlov.tm.util.TerminalUtil;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void create() {
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task item = taskService.create(name, description);
        if (item == null) {
            System.out.println("\tError!");
        } else {
            System.out.println("\tOk");
        }
    }

    @Override
    public void clear() {
        System.out.println("Clear task");
        taskService.clear();
        System.out.println("\tOk");
    }

    @Override
    public void show() {
        int index = 1;
        final List<Task> items = taskService.findAll();
        System.out.println("Tasks list:");
        for (final Task item : items) {
            System.out.println("\t\t" + index + ".\t" + item);
            index++;
        }
        System.out.println("\tOk");
    }

}
