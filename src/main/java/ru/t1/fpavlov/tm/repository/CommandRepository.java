package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.ICommandRepository;
import ru.t1.fpavlov.tm.model.Command;

import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;


/*
 * Created by fpavlov on 04.10.2021.
 */
public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(
            CMD_ABOUT,
            CMD_SHORT_ABOUT,
            "Display developer info"
    );

    private final static Command HELP = new Command(
            CMD_HELP,
            CMD_SHORT_HELP,
            "Display list of terminal command"
    );

    private final static Command VERSION = new Command(
            CMD_VERSION,
            CMD_SHORT_VERSION,
            "Display program version"
    );

    private final static Command EXIT = new Command(
            CMD_EXIT,
            "Quite"
    );

    private final static Command INFO = new Command(
            CMD_INFO,
            CMD_SHORT_INFO,
            "Display available resource of system"
    );

    private final static Command ARGUMENTS = new Command(
            CMD_ARGUMENTS,
            CMD_SHORT_ARGUMENTS,
            "Display arguments of running"
    );

    private final static Command COMMANDS = new Command(
            CMD_COMMANDS,
            CMD_SHORT_COMMANDS,
            "Display names of available commands"
    );

    private final static Command PROJECT_CREATE = new Command(
            CMD_PROJECT_CREATE,
            CMD_SHORT_PROJECT_CREATE,
            "Create new project"

    );

    private final static Command PROJECT_CLEAR = new Command(
            CMD_PROJECT_CLEAR,
            CMD_SHORT_PROJECT_CLEAR,
            "Clear project list"
    );

    private final static Command PROJECT_LIST = new Command(
            CMD_PROJECT_LIST,
            CMD_SHORT_PROJECT_LIST,
            "Display available projects"
    );

    private final static Command TASK_CREATE = new Command(
            CMD_TASK_CREATE,
            CMD_SHORT_TASK_CREATE,
            "Create new task"

    );

    private final static Command TASK_CLEAR = new Command(
            CMD_TASK_CLEAR,
            CMD_SHORT_TASK_CLEAR,
            "Clear task list"
    );

    private final static Command TASK_LIST = new Command(
            CMD_TASK_LIST,
            CMD_SHORT_TASK_LIST,
            "Display available tasks"
    );

    private final static Command[] TERMINAL_COMMANDS = new Command[]{
            ABOUT, HELP, VERSION, EXIT, INFO, ARGUMENTS, COMMANDS,
            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, TASK_CREATE,
            TASK_CLEAR, TASK_LIST
    };

    @Override
    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
