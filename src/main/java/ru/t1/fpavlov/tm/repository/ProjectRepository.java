package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.IProjectRepository;
import ru.t1.fpavlov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public Project add(final Project project) {
        this.projects.add(project);
        return project;
    }

    @Override
    public void clear() {
        this.projects.clear();
    }

    @Override
    public List<Project> findAll() {
        return this.projects;
    }

}
