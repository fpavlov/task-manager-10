package ru.t1.fpavlov.tm.repository;

import ru.t1.fpavlov.tm.api.ITaskRepository;
import ru.t1.fpavlov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public final class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public Task add(final Task project) {
        this.tasks.add(project);
        return project;
    }

    @Override
    public void clear() {
        this.tasks.clear();
    }

    @Override
    public List<Task> findAll() {
        return this.tasks;
    }

}
