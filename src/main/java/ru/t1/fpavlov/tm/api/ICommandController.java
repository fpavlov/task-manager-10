package ru.t1.fpavlov.tm.api;

/*
 * Created by fpavlov on 06.10.2021.
 */
public interface ICommandController {

    void displayArguments();

    void displayCommands();

    void displayWelcomeText();

    void displayHelp();

    void displayAbout();

    void displayVersion();

    void incorrectCommand();

    void incorrectArgument();

    void displaySystemInfo();

}
