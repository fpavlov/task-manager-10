package ru.t1.fpavlov.tm.api;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskController {

    void create();

    void clear();

    void show();

}
