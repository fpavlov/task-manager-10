package ru.t1.fpavlov.tm.api;

import ru.t1.fpavlov.tm.model.Command;

/*
 * Created by fpavlov on 04.10.2021.
 */
public interface ICommandRepository {

    Command[] getTerminalCommands();

}
