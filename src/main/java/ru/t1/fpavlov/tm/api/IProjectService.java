package ru.t1.fpavlov.tm.api;

import ru.t1.fpavlov.tm.model.Project;

import java.util.List;

/**
 * Created by fpavlov on 10.10.2021.
 */
public interface IProjectService {

    Project add(Project project);

    Project create(String name);

    Project create(String name, String description);

    void clear();

    List<Project> findAll();

}
