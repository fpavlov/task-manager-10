package ru.t1.fpavlov.tm.api;

import ru.t1.fpavlov.tm.model.Task;

import java.util.List;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface ITaskRepository {

    Task add(Task project);

    void clear();

    List<Task> findAll();

}
