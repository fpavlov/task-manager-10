package ru.t1.fpavlov.tm.component;

import ru.t1.fpavlov.tm.api.*;
import ru.t1.fpavlov.tm.controller.*;
import ru.t1.fpavlov.tm.repository.*;
import ru.t1.fpavlov.tm.service.*;
import ru.t1.fpavlov.tm.util.TerminalUtil;


import static ru.t1.fpavlov.tm.constant.ArgumentConst.*;
import static ru.t1.fpavlov.tm.constant.TerminalConst.*;

/*
 * Created by fpavlov on 06.10.2021.
 */
public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private void listenerCommand(final String command) {
        switch (command) {
            case CMD_HELP:
                commandController.displayHelp();
                break;
            case CMD_VERSION:
                commandController.displayVersion();
                break;
            case CMD_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case CMD_PROJECT_LIST:
                projectController.show();
                break;
            case CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case CMD_TASK_CREATE:
                taskController.create();
                break;
            case CMD_TASK_LIST:
                taskController.show();
                break;
            case CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case CMD_EXIT:
                quite();
                break;
            default:
                commandController.incorrectCommand();
        }
    }

    private void listenerArgument(final String argument) {
        switch (argument) {
            case CMD_SHORT_HELP:
                commandController.displayHelp();
                break;
            case CMD_SHORT_ABOUT:
                commandController.displayAbout();
                break;
            case CMD_SHORT_VERSION:
                commandController.displayVersion();
                break;
            case CMD_SHORT_INFO:
                commandController.displaySystemInfo();
                break;
            case CMD_SHORT_COMMANDS:
                commandController.displayCommands();
                break;
            case CMD_SHORT_ARGUMENTS:
                commandController.displayArguments();
                break;
            case CMD_SHORT_PROJECT_CREATE:
                projectController.create();
                break;
            case CMD_SHORT_PROJECT_LIST:
                projectController.show();
                break;
            case CMD_SHORT_PROJECT_CLEAR:
                projectController.clear();
                break;
            case CMD_SHORT_TASK_CREATE:
                taskController.create();
                break;
            case CMD_SHORT_TASK_LIST:
                taskController.show();
                break;
            case CMD_SHORT_TASK_CLEAR:
                taskController.clear();
                break;
            default:
                commandController.incorrectArgument();
        }

        System.exit(0);
    }

    public void run(final String[] args) {
        if (areArgumentsAvailable(args)) {
            listenerArgument(args[0]);
            return;
        }

        interactiveCommandProcessing();
    }

    private void interactiveCommandProcessing() {
        commandController.displayWelcomeText();
        while (true) {
            System.out.println("-- Please enter a command --");
            final String param = TerminalUtil.nextLine();
            listenerCommand(param);
        }
    }

    private static boolean areArgumentsAvailable(final String[] args) {
        return (args != null && args.length > 0);
    }

    private static void quite() {
        System.exit(0);
    }

}
