# TASK-MANAGER

## DEVELOPER

**NAME**: Pavlov Philipp

**E-MAIL**: fpavlov@t1-consulting.ru 

## SOFTWARE

**Java**: JDK 1.8

**OS**: Windows10 (Assembly 14393)

## HARDWARE

**CPU**: i5

**RAM** 8GB

**SSD**: 128GB

## APPLICATION RUN

```cmd
    java -jar task-manager.ja
```

## APPLICATION BUILD

```
    mvn clean install
```